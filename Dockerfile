FROM openjdk:8-jre-alpine

EXPOSE 8080

# COPY ./target/java-maven-app-1.1.0-SNAPSHOT.jar /usr/app/
COPY ./target/java-maven-app-*.jar /usr/app/
WORKDIR /usr/app

#Changing java-maven-app-1.0-SNAPSHOT.jar to java-maven-app-*.jar won't work because those are strings that are putted into ENTRYPOINT.
# ENTRYPOINT ["java", "-jar", "java-maven-app-1.0-SNAPSHOT.jar"]

#In order to regular expression, we need to execute this as a command -> use CMD.
CMD java -jar java-maven-app-*.jar