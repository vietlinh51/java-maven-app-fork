def incrementVersion() {
    echo "incrementing app version ..."
    sh "mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit"
    def matcher = readFile('pom.xml') =~ "<version>(.+)</version>"
    def version = matcher[0][1] //For example this would be some kind of 1.1.1
    env.IMAGE_NAME = "$version-$BUILD_NUMBER" //$BUILD_NUMBER is the appeared number when we click Build button.
    env.IMAGE='linhnv51/testing:${IMAGE_NAME}'
}   

def buildJar() {
    echo "building the application..."
    // sh 'mvn package' 
    // When using increment version, there may be many jar files in target folder.
    // So if we use "COPY ./target/java-maven-app-*.jar /usr/app/" in Dockerfile, it doesn't know which jar file to be chosen.
    // Thus, we need to clean the target folder before packaging in order to make sure there is only one jar file exist.
    // To clean target folder then package we use the following command
    sh 'mvn clean package'
} 

def testApp() {
    echo "Testing the application ..."
    echo "Executing pipeline for branch $BRANCH_NAME"
}

// def buildImage() {
//     echo "building the docker image..."
//     withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
//         sh 'docker build -t linhnv51/testing:${IMAGE_NAME} .'
//         sh "echo $PASS | docker login -u $USER --password-stdin"
//         sh 'docker push linhnv51/testing:${IMAGE_NAME}'
//     }
// } 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push ${IMAGE}"
    }
} 

// def deployApp() {
//     echo 'deploying the application...'
//     def dockerCmd = 'docker run -d linhnv51/testing:${IMAGE_NAME}'
//     sshagent(['docker-server']) {
//         sh "ssh -o StrictHostKeyChecking=no ec2-user@172.31.27.238 ${dockerCmd}"
//     }
// } 

def deployApp() {
    echo 'deploying docker image to EC2...'
    def shellCmd = "bash ./docker-server-cmds.sh ${IMAGE}"
    def ec2Instance = "ec2-user@172.31.27.238"
    sshagent(['docker-server']) {
        sh "scp docker-server-cmds.sh ${ec2Instance}:/home/ec2-user"
        sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
    }
} 

//When we build in Jenkins, the artifact version doesn't change, only the build number change.
//The reason is it only increment version the code locally on jenkins container, not for the code in Gitlab.
//So in order to sync the version in pom.xml between jenkins container locally and Gitlab we need to commit the changed pom.xml to Gitlab.
def commitVersionUpdate() {
    withCredentials([usernamePassword(credentialsId: '98a7beab-0c49-450c-b159-022161076c9d', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "git config --global user.email 'jenkins@example.com'"
        sh "git config --global user.name 'jenkins'"
        sh "git status"
        sh "git branch"
        sh "git config --list"
        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/vietlinh51/java-maven-app-fork.git"
        sh "git add pom.xml"
        sh "git status"
        sh "git commit -m 'update artifact version in pom.xml'"
        sh "git push origin HEAD:master"
    }
}

return this